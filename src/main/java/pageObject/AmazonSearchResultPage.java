package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AmazonSearchResultPage {

    public static List<WebElement> getAllTheSearchItems(WebDriver driver){
        return driver.findElements(By.className("s-item-container"));
    }

    public static int getItemValueInINR(WebElement element){
        return Integer.parseInt(element.findElement(By.className("s-price")).getText().replace(" ","").replace(",",""));
    }

    public static String getItemDetails(WebElement element){
        return element.findElement(By.className("s-access-detail-page")).getAttribute("href");
    }


}
