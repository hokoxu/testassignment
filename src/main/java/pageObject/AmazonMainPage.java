package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AmazonMainPage {

    public static WebElement mainSearchBar(WebDriver driver){
        return driver.findElement(By.id("twotabsearchtextbox"));
    }

    public static WebElement searchButton(WebDriver driver){
        return driver.findElement(By.xpath("//*[@id=\"nav-search\"]/form/div[2]/div/input"));
    }

}
