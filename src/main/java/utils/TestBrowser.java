package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestBrowser {


    public static WebDriver getBrowser() {
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            System.setProperty("webdriver.gecko.driver", Constant.path_gecko_windows);
        } else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            System.setProperty("webdriver.gecko.driver", Constant.path_gecko_mac);
        } else {
            System.setProperty("webdriver.gecko.driver", Constant.path_gecko_linux);
        }
        return new FirefoxDriver();
    }
}
