package TestScripts;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Assigenment2 {

    private static String openWeatherApiEndPoint = "https://samples.openweathermap.org/data/2.5/forecast?q=<<searchString>>&appid=b6907d289e10d714a6e88b30761fae22";
    private static String replaceChar = "<<searchString>>";

    public static void willItRainthisWeek(String lat, String lng, String cityName) {

        String tempresults = getUrlContent(openWeatherApiEndPoint.replace(replaceChar, cityName));

        try {
            JsonParser parser = new JsonParser();
            JsonElement jsonResults = parser.parse(tempresults);
            System.out.println(willRain(jsonResults));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static String willRain(JsonElement jsArr) {
        String willRain = "no";
        for (int i = 0; i < jsArr.getAsJsonObject().getAsJsonArray("list").size(); i++) {
            if (jsArr.getAsJsonObject().getAsJsonArray("list").get(i).getAsJsonObject().getAsJsonArray("weather").get(0).getAsJsonObject().get("main").getAsString().contains("Rain")) {
                willRain = jsArr.getAsJsonObject().getAsJsonArray("list").get(i).getAsJsonObject().getAsJsonArray("weather").get(0).getAsJsonObject().get("main").getAsString();
                break;
            } else {
                System.out.println(jsArr.getAsJsonObject().getAsJsonArray("list").get(i).getAsJsonObject().getAsJsonArray("weather").get(0).getAsJsonObject().get("main").getAsString());
            }
        }

        return willRain;
    }

    public static String getUrlContent(String testUrl) {
        StringBuffer result = new StringBuffer();
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(testUrl);
            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    public static String isWeatherClean(JSONObject jsobj) {

        String description = "";
        try {
            JSONObject jsobj1 = (JSONObject) new JSONParser().parse(jsobj.get("weather").toString());
            description = jsobj1.get("description").toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return description;
    }
}
