package TestScripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObject.AmazonMainPage;
import pageObject.AmazonSearchResultPage;
import utils.TestBrowser;

import java.util.TreeMap;

public class Assigenment1 {

    public static void findCheapestItem(String url, String item){

        TreeMap<Integer, String> completeProductList = new TreeMap<Integer, String>();

        WebDriver driver = TestBrowser.getBrowser();
        driver.get(url);
        AmazonMainPage.mainSearchBar(driver).sendKeys(item);
        AmazonMainPage.searchButton(driver).click();


        for(WebElement element : AmazonSearchResultPage.getAllTheSearchItems(driver)){
            try{
                System.out.println(AmazonSearchResultPage.getItemDetails(element)+"\t"+AmazonSearchResultPage.getItemValueInINR(element));
                completeProductList.put(AmazonSearchResultPage.getItemValueInINR(element),AmazonSearchResultPage.getItemDetails(element));
            }catch (Exception e){
                try{
                    System.out.println(AmazonSearchResultPage.getItemDetails(element)+"\tItem Not Available");
                }catch (Exception e1){
                    continue;
                }

            }

        }

        String cheapestItemUrl = completeProductList.get(completeProductList.firstKey());

        driver.get(cheapestItemUrl);
        driver.findElement(By.id("add-to-cart-button")).click();

        driver.close();

    }
}
